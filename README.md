# *rversions*'s readme

## Description

*rversions* is a complete rewrite of
[versions](https://github.com/dupgit/versions)
in Rust. It is an open source (GPL v3) software that checks
releases and versions of your favourite programs through RSS
or Atom feeds and tells you which one has been released since
the last time is has been checked.

It can check projects from any website or repository that
provides an RSS or an Atom feed to track releases or tags
that may contain versions. This includes, but is not limited to
github.com, sourceforge.net, freshcode.club, pypi.python.org,
savanah.gnu.org, www.freshports.org, fossies.org, repo.continuum.io,
sourcehut forge, gitlab forge, crates.io.
Projects must be added to a YAML file (by default
`~/.config/rversions/rversions.yaml`).

Unlike versions, *rversions* tracks major versions
independently: if a `22.4.3` is created after a `23.2.3`
and previous versions were `22.4.2` and `23.1.9` it will
be taken into account and both will be printed and
remembered.

*rversions* uses and produces text files. Those files are
cache files written into `~/.cache/rversions` directory.
`*.cache` cache files are containing the project list and
their associated versions (the latest in each sub version
if any), the date at which the release occurred and the
date when *rversions* checked it.


## Installation

To install *rversions* you need first to install
[Rust](https://www.rust-lang.org/tools/install).

### From crates.io

Use cargo to directly install *rversions* for your system:
`cargo install rversions` (depending on your system this
may take some time - on mine it takes nearly 5 minutes).

### From source

Use git to clone the repository and build the release
binary of *rversions*:

```bash
git clone https://gitlab.com/delhomme/rversions.git
cd rversions
cargo build --release
```

This will compile all needed dependencies and at last will
produce a binary program in `target/release/` named `rversions`.
One may call it directly to use it:

`./target/release/rversions --help`


## YAML configuration file structure

By default *rversions* will try to load it's configuration
file from `~/.config/rversions/rversions.yaml` if it exists.
Such file is a list of sites definitions and must comply to
the following format:


```yaml
sites:
  - name: 'FreshCode'
    url: 'https://freshcode.club/projects.rss'
    type: list
    projects:
      - name: FileZilla
        filter-in: 'v([\d\.\-rRcC]+)'
      - name: sauvegarde
      - name: git

  - name: 'pypi'
    url: 'https://pypi.org/rss/updates.xml'
    type: list
    projects:
      - name: PyYAML
      - name: feedparser

  - name: 'github'
    url: "https://github.com/{}/tags.atom"
    type: byproject
    projects:
      - name: hashicorp/terraform
        filter-in: 'v([\d\.-rRcCbBeEtTaAlLpPhH]+)'
      - name: ANSSI-FR/MLA
        filter-in: 'mlar-v([\d\.]+)'
      - name: StackStorm/st2
        entry: 'last checked'
      - name: gohugoio/hugo
        filter-in: '(\d+.\d+.\d+[\.\-\+\w]+)'
        entry: 'last checked'
```

Sites must have a name, an url and a type. There is two types
of sites:

* `list`: The site has one feed with all projects in it such
   as [freshcode.club](http://freshcode.club/) or
   [fossies.org](https://fossies.org/). Those sites are
   expected to publish their projects in the same format
   between posts and a site level `filter-in:` regular expression
   can be defined. This regular expression must have two matching
   groups: one for project's name and one for it's version.
   Similarly site level `entry:` can be defined to print either
   latest entry or all entries found from last checked time.
   A site level `multiproject:` field can be defined in case
   one post contains more than one project update (for instance
   `mxnet: 1.9.1 b0; libmxnet: 1.9.1 b0; mxnet-openblas: 1.9.1 b0`.
   It as to be an array of characters delimiters for each projects
   such as  `[',', '|', ';']`.
* `byproject`: The site gives access to one feed per project
   such as [gitlab](https://gitlab.com/delhomme/rversions) for
   instance. Brackets `{}` represents the name of the project
   as found in the `projects:` list for the site. Those projects
   can be listed directly with `name:` field and may take options.
   You can specify either a `filter-in:`, `filter-out:` or an
   `entry:` type option (or all at the same time).

Common fields that may be defined:

* `filter-in:` is a [regular expression](https://docs.rs/regex/latest/regex/#syntax)
  used to match version number which will then be used to sort
  versions among themselves.
* `filter-out:` is a regular expression used to exclude some
  versions (for instance all `-rc` versions) from being
  reported.
* `entry:` is used to determine if versions has to print the
  latest entry (default behavior) or all new entries from the
  `last checked` date.

A real life example file is provided: [examples/versions.yaml](examples/versions.yaml)


## Usage

*rversions* is a command line tool that may be invoked directly in a
terminal or in scripts. Note that *rversions* is compliant with
[NO_COLOR](https://no-color.org/) specification. You can have full
usage options invoking `rversions` command with `--help` flag. It
will give you something similar to:

```text
    -d, --debug                  Starts in debug mode and prints things that may help
    -f, --filename <FILENAME>    Configuration file filename (in YAML format) with projects to check
    -h, --help                   Print help information
    -l, --list                   List all projects and their version in cache
    -V, --version                Print version information
```

For instance to test the real life example you can do:
`./target/release/rversions -f examples/versions.yaml`


## Related links

* [NewRelease](https://newreleases.io/)
* [SemVer](https://semver.org/)
