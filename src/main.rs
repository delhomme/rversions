/* -*- coding: utf8 -*-
 *
 *  main.rs : checks releases and versions of programs through RSS
 *            or Atom feeds and tells you
 *
 *  (C) Copyright 2021 - 2023 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software Foundation,
 *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#![doc = include_str!("../README.md")]
#![forbid(unsafe_code)]
mod cached;
mod site;
mod utils;
mod version;

#[macro_use]
extern crate log;
use clap::Parser;
use dirs::{cache_dir, config_dir};
use env_logger::{Env, WriteStyle};
use site::{read_site_config_file, ConfigPaths};
use std::env::var;
use std::path::PathBuf;
use utils::{ensure_this_directory_exists, print_and_exit};

// This structure holds arguments provided to the program from the command line.
// filename: Configuration file filename (in YAML format) with projects to
//           check. YAML must follow [Sites](site/struct.Sites.html) structure.
// list: true makes the program list all projects and version already in its cache.
// debug: true puts debug mode on (the program prints more information that may be
//        useful.
#[derive(Parser, Debug)]
/// This program checks releases and versions of programs through RSS or Atom feed.
#[command(version)]
struct Args {
    /// Configuration file filename (in YAML format) with projects to check.
    #[arg(short, long)]
    filename: Option<String>,

    /// List projects and their version in cache. PROJECT may be a project name to filter out this particular project.
    #[arg(short, long, value_name = "PROJECT")]
    list: Option<Option<String>>,

    /// When displaying project's versions also display the associated date.
    #[arg(short, long, requires = "list", default_value_t = false)]
    date: bool,

    /// Starts in debug mode and prints things that may help.
    #[arg(short = 'D', long)]
    debug: bool,
}

// Ensure that cache and config dirs (by default ~/.cache/rversions and ~/.config/rversions)
// exists.
fn ensure_directories_availability(paths: &ConfigPaths) {
    ensure_this_directory_exists(&paths.config_path);
    ensure_this_directory_exists(&paths.cache_path);
}

fn main() {
    let args = Args::parse();

    /* NO_COLOR compliance */
    let color = match var("NO_COLOR").is_ok() {
        true => WriteStyle::Never,
        false => WriteStyle::Auto,
    };

    if args.debug {
        /* Force debugging */
        env_logger::Builder::from_env(Env::default().default_filter_or("debug")).write_style(color).init();
    } else {
        env_logger::Builder::from_default_env().write_style(color).init();
    }

    let mut cache_path: PathBuf = match cache_dir() {
        Some(p) => p,
        None => panic!("Error getting cache directory."),
    };

    let mut config_path: PathBuf = match config_dir() {
        Some(p) => p,
        None => panic!("Error getting config directory."),
    };

    cache_path.push("rversions");
    config_path.push("rversions");

    let mut paths: ConfigPaths = ConfigPaths {
        cache_path,
        config_path,
    };

    ensure_directories_availability(&paths);

    let filename: String = match args.filename {
        Some(name) => name,
        None => {
            // Adding default configuration filename name to the path.
            paths.config_path.push("rversions.yaml");
            paths.config_path.as_os_str().to_os_string().into_string().unwrap()
        }
    };

    match read_site_config_file(&filename) {
        Ok(sites) => sites.iterate_over_sites(args.list, args.date, paths),
        Err(error) => print_and_exit(&format!("Error reading configuration file '{filename}': {error}"), 1),
    };
}
