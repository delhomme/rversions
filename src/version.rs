/* -*- coding: utf8 -*-
 *
 *  version.rs : implements Version structure and logic.
 *
 *  (C) Copyright 2021 - 2022 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software Foundation,
 *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

extern crate std;

use std::cmp::Ordering;
use std::fmt;
use std::num::ParseIntError;
use std::str::FromStr;

/// Structure that manages a version 'number'.
///
/// * `version`: a String that represents a version  It may not be semver compatible
///              and in this case the field `valid` is set to false. `1.57.7-rc3+12`
///              is considered a valid semver version number.
/// * `major`: a i32 representing the major version number ie `1` in the example above.
/// * `minor`: a i32 representing the minor version number ie `57` in the example above.
/// * `patch`: a i32 representing the patch version number ie `7` in the example above.
/// * `prerelease`: a String that can contain any numbers or letters ie `rc3` in the
///                 example above.
/// * `build`: a String that can contain any numbers or letters ie `12` in the example
///            above.
/// * `valid` is a boolean that is true when the string containing the version number
///           has been successfully decoded and fits in the structure it is false
///           otherwise.
///
/// `major`, `minor` and `patch` can take negatives values:
/// * `-1` indicates the absence of the value ie `1.0` has no patch value so patch value will
///        be set to `-1`
/// * `-2` indicates an error when trying to get a numeric value ie `1.0.2b`, `2b` is not a
///        valid number so patch value is set to `-2`

#[derive(Debug, Default, Eq)]
pub struct Version {
    pub version: String,
    pub major: i32,
    pub minor: i32,
    pub patch: i32,
    pub prerelease: String,
    pub build: String,
    pub valid: bool,
}

fn parse_str_to_number(num_vector: &[&str], index: usize, vector_len: usize) -> i32 {
    if index < vector_len {
        num_vector[index].parse().unwrap_or(-2)
    } else {
        -1
    }
}

impl Version {
    /// Takes a String that should contain a version 'number': "1.57.7-rc3" for
    /// instance. To be valid this String must follow the
    /// [semver](https://semver.org/) specification:
    ///
    /// ```
    /// <valid semver> ::= <version core>
    ///                  | <version core> "+" <build>
    ///                  | <version core> "-" <pre-release>
    ///                  | <version core> "-" <pre-release> "+" <build>
    ///
    /// <version core> ::= <major> "." <minor> "." <patch>
    /// ```
    ///
    /// # Example
    /// A vim version (2021/12/31) can be created with `new()` function:
    /// `Version::new("8.2.3959".to_string());`
    pub fn new(version: &str) -> Version {
        /* Extracting build version if any: core+build */
        let (core, build) = match version.split_once('+') {
            Some((c, s)) => (c, s),
            None => (version, ""),
        };

        /* Extracting prerelease version if any: dottedcore-prelease */
        let (dottedcore, prerelease) = match core.split_once('-') {
            Some((d, p)) => (d, p),
            None => (core, ""),
        };

        /* Extracting dotted part of the version: major. minor. patch */
        let num_vector: Vec<&str> = dottedcore.split('.').collect();
        let vector_len = num_vector.len();
        let major: i32 = parse_str_to_number(&num_vector, 0, vector_len);
        let minor: i32 = parse_str_to_number(&num_vector, 1, vector_len);
        let patch: i32 = parse_str_to_number(&num_vector, 2, vector_len);

        let valid = major != -2 && minor != -2 && patch != -2 && !num_vector.is_empty() && vector_len <= 3;

        Version {
            version: version.to_string(),
            major,
            minor,
            patch,
            prerelease: prerelease.to_string(),
            build: build.to_string(),
            valid,
        }
    }
}

/*** Traits ***/
/// `FromStr` trait lets you write: `let a_version: Version = "8.2.3959".parse().unwrap();`
impl FromStr for Version {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Version::new(s))
    }
}

/// Display trait for Version. It will display the String as is
/// even if version number is invalid.
impl fmt::Display for Version {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.version)
    }
}

/// `PartialEq` trait for Version to know if a version number is
/// equal or not to another version number.
/// Version numbers validity is not tested. You may experiment
/// silly things when using invalid version numbers such as:
/// `1.2.1b > 9a.4.5`
impl PartialEq for Version {
    fn eq(&self, other: &Self) -> bool {
        self.major == other.major && self.minor == other.minor && self.patch == other.patch && self.prerelease == other.prerelease && self.build == other.build
    }
}

/// `Ord` trait for Version to allow version ordering.
/// Ordering versions is only considered with dotted numbers first then prerelease
/// strings are compared (as strings). A version with a prerelease string is considered
/// to be less than the same version without this prerelease string (ie `1.0.0-rc1 < 1.0.0`)
/// Prereleased are compared using string comparison and one would expect `rc1 < rc2`.
/// build version are not used for comparison for now
///
/// # Order examples:
/// ```
/// 8.2.3959 > 8.2.3951
/// 8.3.1 > 8.2.3959
/// 9.0.0 > 8.3.1
/// 8.5.9 > 8.5.9-rc1
/// 8.5.9-rc7 > 8.5.9-rc1
/// ```
///
/// Valid version number is not taking into account so you may experiment silly
/// ordering when using invalid version numbers such as `1.2.1b > 9a.4.5`
impl Ord for Version {
    fn cmp(&self, other: &Self) -> Ordering {
        let cmpr = self.major.cmp(&other.major);
        if cmpr == Ordering::Equal {
            let cmpr = self.minor.cmp(&other.minor);
            if cmpr == Ordering::Equal {
                let cmpr = self.patch.cmp(&other.patch);
                if cmpr == Ordering::Equal {
                    if self.prerelease.is_empty() && other.prerelease.is_empty() {
                        /* same version numbers and prerelease                        */
                        /* @todo: see how to compare build string if any in this case */
                        Ordering::Equal
                    } else if self.prerelease.is_empty() {
                        Ordering::Greater
                    } else if other.prerelease.is_empty() {
                        Ordering::Less
                    } else {
                        /* @todo: see how to compare build string if any in this case */
                        self.prerelease.cmp(&other.prerelease)
                    }
                } else {
                    cmpr
                }
            } else {
                cmpr
            }
        } else {
            cmpr
        }
    }
}

/// `PartialOrd` trait for Version to allow ordering.
impl PartialOrd for Version {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/*** Tests ***/
#[cfg(test)]
mod tests {

    use crate::version::Version;
    use std::cmp::Ordering;

    #[test]
    fn testing_version_new() {
        let a_version = Version::new("1.57.0-night+alpha");
        assert_eq!(
            a_version,
            Version {
                version: "1.57.0-night+alpha".to_string(),
                major: 1,
                minor: 57,
                patch: 0,
                prerelease: "night".to_string(),
                build: "alpha".to_string(),
                valid: true
            }
        );

        let a_version = Version::new("1.57.0-alpha");
        assert_eq!(
            a_version,
            Version {
                version: "1.57.0-alpha".to_string(),
                major: 1,
                minor: 57,
                patch: 0,
                prerelease: "alpha".to_string(),
                build: "".to_string(),
                valid: true
            }
        );

        let a_version = Version::new("1b.57a.0alpha");
        assert_eq!(
            a_version,
            Version {
                version: "1b.57a.0alpha".to_string(),
                major: -2,
                minor: -2,
                patch: -2,
                prerelease: "".to_string(),
                build: "".to_string(),
                valid: false
            }
        );

        let a_version = Version::new("1.57-0alpha");
        assert_eq!(
            a_version,
            Version {
                version: "1.57-0alpha".to_string(),
                major: 1,
                minor: 57,
                patch: -1,
                prerelease: "0alpha".to_string(),
                build: "".to_string(),
                valid: true
            }
        );

        let a_version = Version::new("No version at all here. Right ?");
        assert_eq!(
            a_version,
            Version {
                version: "No version at all here. Right ?".to_string(),
                major: -2,
                minor: -2,
                patch: -1,
                prerelease: "".to_string(),
                build: "".to_string(),
                valid: false
            }
        );
    }

    #[test]
    fn testing_fromstr_trait() {
        let a_version: Version = "1.57.0-night+alpha".parse().unwrap();
        assert_eq!(
            a_version,
            Version {
                version: "1.57.0-night+alpha".to_string(),
                major: 1,
                minor: 57,
                patch: 0,
                prerelease: "night".to_string(),
                build: "alpha".to_string(),
                valid: true
            }
        );
    }

    #[test]
    fn testing_display_trait() {
        let a_version: Version = "9.99.200".parse().unwrap();
        assert_eq!(a_version.to_string(), "9.99.200".to_string());
    }

    #[test]
    fn testing_partialeq_trait_1() {
        let a_version0: Version = "8.2.3959".parse().unwrap();
        let a_version1: Version = "8.2.3951".parse().unwrap();

        assert_ne!(a_version0, a_version1);
        assert_eq!(a_version0, a_version0);
    }

    #[test]
    #[should_panic(
        expected = "assertion `left == right` failed\n  left: Version { version: \"8.2.3959\", major: 8, minor: 2, patch: 3959, prerelease: \"\", build: \"\", valid: true }\n right: Version { version: \"8.2.3951\", major: 8, minor: 2, patch: 3951, prerelease: \"\", build: \"\", valid: true }"
    )]
    fn testing_partialeq_trait_2() {
        let a_version0: Version = "8.2.3959".parse().unwrap();
        let a_version1: Version = "8.2.3951".parse().unwrap();

        assert_eq!(a_version0, a_version1);
    }

    #[test]
    fn testing_ord_trait() {
        let a_version0: Version = "8.2.3959".parse().unwrap();
        let a_version1: Version = "8.2.3951".parse().unwrap();
        let invalid1: Version = "1.2.1b".parse().unwrap();
        let invalid2: Version = "9a.4.5".parse().unwrap();
        let tmux1: Version = "3.3".parse().unwrap();
        let tmux2: Version = "3.3a".parse().unwrap();

        assert_eq!(a_version0.cmp(&a_version1), Ordering::Greater);
        assert_eq!(a_version1.cmp(&a_version0), Ordering::Less);
        assert_eq!(a_version1.cmp(&a_version1), Ordering::Equal);
        assert_eq!(invalid1.cmp(&invalid2), Ordering::Greater); // silly things !
        assert_eq!(tmux1.cmp(&tmux2), Ordering::Greater);

        let a_version0: Version = "8.2.3959-rc1".parse().unwrap();
        let a_version1: Version = "8.2.3959".parse().unwrap();
        assert_eq!(a_version0.cmp(&a_version1), Ordering::Less);

        let a_version0: Version = "8.2.3959-rc7".parse().unwrap();
        let a_version1: Version = "8.2.3959-rc2".parse().unwrap();
        assert_eq!(a_version0.cmp(&a_version1), Ordering::Greater);

        let a_version0: Version = "8.2.3959".parse().unwrap();
        let a_version1: Version = "8.2.3959-rc2".parse().unwrap();
        assert_eq!(a_version0.cmp(&a_version1), Ordering::Greater);
    }
}
