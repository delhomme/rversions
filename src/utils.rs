/* -*- coding: utf8 -*-
 *
 *  main.rs : checks releases and versions of programs through RSS
 *            or Atom feeds and tells you
 *
 *  (C) Copyright 2021 - 2022 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software Foundation,
 *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

use std::error::Error;
use std::fs::create_dir_all;
use std::path::PathBuf;
use std::process::exit;

pub fn print_and_exit(message: &str, exit_code: i32) {
    match exit_code {
        0 => println!("{message}"),
        _ => eprintln!("{message}"),
    };
    exit(exit_code);
}

// Creates a directory if it does not exists. Returns Ok unless create_dir_all() fails.
fn create_dir_if_needed(path: &PathBuf) -> Result<(), Box<dyn Error>> {
    if !path.exists() {
        create_dir_all(path)?;
    }

    Ok(())
}

// ensures that directory exists by calling create_dir_if_needed() function
pub fn ensure_this_directory_exists(directory: &PathBuf) {
    match create_dir_if_needed(directory) {
        Err(e) => print_and_exit(&format!("Error while creating directory '{directory:?}': {e}"), 1),
        Ok(_) => debug!("Directory '{:?}' exists.", directory),
    };
}
