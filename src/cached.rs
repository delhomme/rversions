/* -*- coding: utf8 -*-
 *
 *  cached.rs : Implements logic to get information in cached files.
 *
 *  (C) Copyright 2021 - 2023 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software Foundation,
 *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

use crate::site::{FoundVersion, GetEntry};
use crate::version::Version;
use chrono::prelude::*;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::error::Error;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::path::Path;

#[derive(Serialize, Deserialize, Debug)]
struct SpecificVersion {
    version: String,
    date: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct CachedProgram {
    name: String,
    date: String,
    versions: Vec<SpecificVersion>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct CachedVersions {
    projects: Vec<CachedProgram>,
}

pub fn read_cached_file(filename: &Path) -> Result<CachedVersions, Box<dyn Error>> {
    debug!("Loading cached file: {:?}", filename);

    let mut contents = String::new();

    let content = match OpenOptions::new().read(true).write(false).open(filename) {
        Ok(mut file) => {
            file.read_to_string(&mut contents)?;
            contents
        }
        Err(error) => {
            /* If a file does not exists it may not be an error as we
             * will create it when saving the cached information.
             */
            if error.kind() == std::io::ErrorKind::NotFound {
                contents
            } else {
                return Err(Box::new(error));
            }
        }
    };

    let cached: CachedVersions = if content.is_empty() {
        CachedVersions::new()
    } else {
        serde_yml::from_str(&content)?
    };

    Ok(cached)
}

pub fn write_cached_file(filename: &Path, cached: &CachedVersions) -> Result<(), Box<dyn Error>> {
    debug!("Writing cached file: {:?}", filename);

    let contents = serde_yml::to_string(cached)?;
    OpenOptions::new().read(false).write(true).create(true).truncate(true).open(filename)?.write_all(contents.as_bytes())?;

    Ok(())
}

/// pts stands for project to search
pub fn print_cached_information(cached: &CachedVersions, pts: &str, date: bool) {
    for project in &cached.projects {
        if pts == "all" || project.name.contains(pts) {
            debug!("{} {}:", project.name, project.date);
            println!("{}:", project.name);
            for version in &project.versions {
                debug!("\t{}: {}", version.version, version.date);
                if date {
                    println!("\t{}: \t{}", version.version, version.date);
                } else {
                    println!("\t{}", version.version);
                }
            }
        }
    }
    if pts == "all" {
        println!();
    }
}

fn parse_date_from_rfc3339_string(date: &str) -> DateTime<FixedOffset> {
    let local_date: DateTime<Local> = Local::now();
    match DateTime::parse_from_rfc3339(date) {
        Ok(d) => d,
        Err(_) => local_date.with_timezone(&FixedOffset::east_opt(3600).unwrap()),
    }
}

fn print_version(name: &str, version: &str) {
    println!("{name}: {version}");
}

fn update_version_structure(vers: &mut SpecificVersion, prgname: &str, v1: &Version, trouve: &FoundVersion) {
    vers.version = trouve.version.to_string();
    vers.date = trouve.published.to_string();
    print_version(prgname, &vers.version);
    debug!("Updated in cache: {} from version {} to version {} ({})", prgname, v1.version, vers.version, vers.date);
}

fn print_if_newer_than_last_checked(prgname: &str, trouve: &FoundVersion, program_date: &str) {
    if let GetEntry::LastChecked = &trouve.entry {
        let date1 = parse_date_from_rfc3339_string(&trouve.published);
        let date2 = parse_date_from_rfc3339_string(program_date); /* This is the date when the program was last checked */

        if date1.timestamp() > date2.timestamp() {
            print_version(prgname, &trouve.version);
        }
    }
}

fn update_version_structure_with_date_cmp(vers: &mut SpecificVersion, prgname: &str, v1: &Version, trouve: &FoundVersion, program_date: &str) {
    let date1 = parse_date_from_rfc3339_string(&trouve.published);
    let date2 = parse_date_from_rfc3339_string(&vers.date);

    debug!("is date1 {} > date2 {} ?", date1.timestamp(), date2.timestamp());

    if date1.timestamp() > date2.timestamp() {
        update_version_structure(vers, prgname, v1, trouve);
    } else {
        print_if_newer_than_last_checked(prgname, trouve, program_date);
    }
}

fn insert_or_not_in_program_version_cache(program: &mut CachedProgram, trouve: &FoundVersion) {
    // let version = &trouve.version;
    // let published = &trouve.published;
    let v1: Version = Version::new(&trouve.version);
    let mut updated = false;

    if !v1.valid {
        /* If version is not valid then we modify the first entry in cache and
         * do not try to keep more than the newest version string
         */
        let vers = &mut program.versions[0];
        update_version_structure_with_date_cmp(vers, &program.name, &v1, trouve, &program.date);
    } else {
        /* update only same major distributions ie keep major branches in cache */
        for vers in &mut program.versions {
            let v2: Version = Version::new(&vers.version);
            if v2.valid {
                /* v1.valid is true here */
                if v1.major == v2.major {
                    match v1.cmp(&v2) {
                        Ordering::Greater => {
                            update_version_structure(vers, &program.name, &v1, trouve);
                        }
                        Ordering::Equal => {
                            /* Only major, minor and release are compared here in v1 == v2 */
                            update_version_structure_with_date_cmp(vers, &program.name, &v1, trouve, &program.date);
                        }
                        Ordering::Less => print_if_newer_than_last_checked(&program.name, trouve, &program.date),
                    }
                    updated = true; /* what ever we did we consider that this version has been updated */
                }
            }
        }

        /* if we did not update before this is a new major number so we need to insert it */
        if !updated {
            debug!("Inserted in cache: {} version {} ({})", program.name, trouve.version, trouve.published);
            let this_version: SpecificVersion = SpecificVersion {
                version: trouve.version.to_string(),
                date: trouve.published.to_string(),
            };
            print_version(&program.name, &trouve.version);
            program.versions.push(this_version);
        }
    }
}

impl CachedVersions {
    pub fn new() -> CachedVersions {
        let projects: Vec<CachedProgram> = Vec::new();
        CachedVersions {
            projects,
        }
    }

    pub fn update_last_checked_date(&mut self) {
        let local_date: DateTime<Local> = Local::now();
        for program in &mut self.projects {
            program.date = local_date.to_rfc3339();
        }
    }

    pub fn update_cache(&mut self, trouve: &FoundVersion) {
        let name = &trouve.name;
        let version = &trouve.version;
        let published = &trouve.published;
        let mut updated = false;
        let versions: Vec<SpecificVersion> = Vec::new();

        for program in &mut self.projects {
            if program.name == *name {
                insert_or_not_in_program_version_cache(program, trouve);
                updated = true;
            }
        }
        if !updated {
            /* The program is not in the cache already: we must insert it in the cache */
            let local_date: DateTime<Local> = Local.timestamp_opt(0, 0).unwrap(); /* A new program in the cache is last checked at epoch (never) ! */
            let mut this_program: CachedProgram = CachedProgram {
                name: name.to_string(),
                date: local_date.to_rfc3339(),
                versions,
            };
            let this_version: SpecificVersion = SpecificVersion {
                version: version.to_string(),
                date: published.to_string(),
            };
            this_program.versions.push(this_version);
            self.projects.push(this_program);
            debug!("Inserted in cache: {} version {} ({})", name, version, published);

            print_version(name, version);
        }
    }
}
