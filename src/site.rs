/* -*- coding: utf8 -*-
 *
 *  site.rs : Implements logic for web sites crawling and their associated
 *            structures and tests.
 *
 *  (C) Copyright 2021 - 2023 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software Foundation,
 *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

extern crate feed_rs;
extern crate std;

use crate::cached::{print_cached_information, read_cached_file, write_cached_file, CachedVersions};
use crate::utils::print_and_exit;
use feed_rs::model::{Entry, Text};
use feed_rs::parser::parse;
use mediatype::{names::PLAIN, names::TEXT, MediaTypeBuf};
use regex::Regex;
use reqwest::blocking::Client;
use reqwest::header::{ACCEPT, USER_AGENT};
use serde::Deserialize;
use std::error::Error;
use std::fs::read_to_string;
use std::path::{Path, PathBuf};
use std::thread::spawn;

/// `entry` is either `latest` (by default) or `last checked`.
/// * `latest` should print only latest version number found even if more
///   than one version change has been detected.
/// * `last checked` should print all versions changes since last date checked.
#[derive(Clone, Debug, Default, Deserialize)]
pub enum GetEntry {
    #[serde(rename = "latest")]
    Latest,
    #[serde(rename = "last checked")]
    LastChecked,
    #[default]
    NotDefined,
}

#[derive(Deserialize, Debug)]
pub struct Program {
    name: String,
    #[serde(default, alias = "filter-in")]
    regex: String,
    #[serde(default)]
    entry: GetEntry,
    #[serde(default, alias = "filter-out")]
    filterout: String,
}

#[derive(Deserialize, Debug)]
pub enum SiteType {
    #[serde(rename = "byproject")]
    Project,
    #[serde(rename = "list")]
    List,
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct Site {
    name: String,
    #[serde(rename = "type")]
    sitetype: SiteType,
    url: String,
    projects: Vec<Program>,
    #[serde(default, alias = "filter-in")]
    regex: String,
    #[serde(default)]
    multiproject: Vec<char>,
    #[serde(default)]
    entry: GetEntry,
    #[serde(default, alias = "filter-out")]
    filterout: String,
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct Sites {
    sites: Vec<Site>,
}

#[derive(Clone)]
pub struct FoundVersion {
    pub name: String,
    pub version: String,
    pub published: String,
    pub entry: GetEntry,
}

pub struct ConfigPaths {
    pub cache_path: PathBuf,
    pub config_path: PathBuf,
}

fn get_entries_from_feed(client: &Client, projecturl: &str) -> Result<Vec<Entry>, Box<dyn Error>> {
    let response = client.get(projecturl).header(ACCEPT, "*/*").header(USER_AGENT, "rversions/0.1.0").send()?;

    if response.status().is_success() {
        let bytes = response.bytes()?;
        match parse(&bytes[..]) {
            Ok(feed) => Ok(feed.entries),
            Err(e) => Err(Box::new(e)),
        }
    } else {
        /* An error fetching one url must not stop the program: it may happen in real life. */
        eprintln!("Error {:?} while fetching '{projecturl}'", response.status());
        Ok(Vec::new())
    }
}

fn get_title_from_entry(entry: Entry) -> (Text, String) {
    let empty: String = "".to_string();
    let empty_title: Text = Text {
        content_type: MediaTypeBuf::new(TEXT, PLAIN),
        src: None,
        content: empty,
    };

    let title: Text = match entry.title {
        Some(texte) => texte,
        None => empty_title,
    };

    let empty: String = "".to_string();
    let published: String = match entry.published {
        Some(d) => d.to_rfc3339(),
        None => match entry.updated {
            Some(u) => u.to_rfc3339(),
            None => empty,
        },
    };

    (title, published)
}

fn capture_with_regex(title: &str, regex: &str) -> Result<(String, String), Box<dyn Error>> {
    let re: Regex = Regex::new(regex)?;
    let (name, version): (String, String) = match re.captures(title) {
        Some(value) => {
            if value.len() > 2 {
                debug!("title: {} ; regex: {} ; name: '{}' ; version: {}", title, regex, value[1].to_string(), value[2].to_string());
                (value[1].to_string(), value[2].to_string())
            } else {
                debug!("title: {} ; regex: {} ; name: '' ; version: {}", title, regex, value[1].to_string());
                ("".to_string(), value[1].to_string())
            }
        }
        None => try_to_split_using_space(title),
    };
    Ok((name, version))
}

/* tries to split the title into two parts : name and version
 * using space as a separator. It does this from the end of
 * the string because we infer that version does not contain
 * a space where the program title may.
 */
fn try_to_split_using_space(title_content: &str) -> (String, String) {
    let split: Vec<&str> = title_content.rsplitn(2, ' ').collect();

    if split.len() == 2 {
        (split[1].to_string(), split[0].to_string())
    } else {
        ("".to_string(), title_content.to_string())
    }
}

fn get_version_from_title_with_regex(title_content: &str, regex: &str, siteregex: &str, program_filterout: &str, site_filterout: &str) -> Result<(String, String), Box<dyn Error>> {
    /* filtering out if title_content matches one of the defined
     * regex site_filterout or program_filterout
     */
    if !site_filterout.is_empty() {
        let re: Regex = Regex::new(site_filterout)?;
        if re.is_match(title_content) {
            debug!("Filtering out: {} ({})", title_content, site_filterout);
            return Ok(("".to_string(), "".to_string()));
        }
    }
    if !program_filterout.is_empty() {
        let re: Regex = Regex::new(program_filterout)?;
        if re.is_match(title_content) {
            debug!("Filtering out: {} ({})", title_content, program_filterout);
            return Ok(("".to_string(), "".to_string()));
        }
    }

    if !regex.is_empty() {
        let (name, version) = capture_with_regex(title_content, regex)?;
        Ok((name, version))
    } else if !siteregex.is_empty() {
        let (name, version) = capture_with_regex(title_content, siteregex)?;
        Ok((name, version))
    } else {
        Ok(try_to_split_using_space(title_content))
    }
}

fn get_versions_from_byproject_site(client: &Client, site: &Site, program: &Program) -> Result<Vec<FoundVersion>, Box<dyn Error>> {
    let projecturl = &site.url.replace("{}", program.name.as_str());

    let entries: Vec<Entry> = get_entries_from_feed(client, projecturl)?;
    let mut found: Vec<FoundVersion> = Vec::new();

    debug!("{}: ({})", &program.name, &program.regex);

    for entry in entries {
        let (title, published) = get_title_from_entry(entry);

        /* On byproject sites the title may contain a version number only
         * It is not expected to contain the program name itself so we know
         * that get_version_from_title_with_regex() will return only the
         * matched version if any.
         */
        match get_version_from_title_with_regex(&title.content, &program.regex, &site.regex, &program.filterout, &site.filterout) {
            Ok((_, mversion)) => {
                if !mversion.is_empty() {
                    found.push(get_foundversion_upon_entry(&program.name, &mversion, &published, &program.entry, &site.entry));
                }
            }
            Err(e) => {
                eprintln!("Error: {e}");
                eprintln!("\t{:#?} ({:#?})", title.content, program.regex);
            }
        };
    }

    Ok(found)
}

fn get_foundversion_upon_entry(name: &str, version: &str, published: &str, entry: &GetEntry, siteentry: &GetEntry) -> FoundVersion {
    debug!("Checking {}: {}", name, version);
    match siteentry {
        GetEntry::NotDefined => match entry {
            GetEntry::NotDefined | GetEntry::Latest => FoundVersion {
                name: name.to_string(),
                version: version.to_string(),
                published: published.to_string(),
                entry: GetEntry::Latest,
            },
            GetEntry::LastChecked => FoundVersion {
                name: name.to_string(),
                version: version.to_string(),
                published: published.to_string(),
                entry: GetEntry::LastChecked,
            },
        },
        GetEntry::Latest => match entry {
            GetEntry::NotDefined | GetEntry::Latest => FoundVersion {
                name: name.to_string(),
                version: version.to_string(),
                published: published.to_string(),
                entry: GetEntry::Latest,
            },
            GetEntry::LastChecked => FoundVersion {
                name: name.to_string(),
                version: version.to_string(),
                published: published.to_string(),
                entry: GetEntry::LastChecked,
            },
        },
        GetEntry::LastChecked => match entry {
            GetEntry::NotDefined | GetEntry::LastChecked => FoundVersion {
                name: name.to_string(),
                version: version.to_string(),
                published: published.to_string(),
                entry: GetEntry::LastChecked,
            },
            GetEntry::Latest => FoundVersion {
                name: name.to_string(),
                version: version.to_string(),
                published: published.to_string(),
                entry: GetEntry::Latest,
            },
        },
    }
}

fn get_entries_and_projects_intersection(entries: Vec<Entry>, projects: &[Program], site: &Site) -> Vec<FoundVersion> {
    let multiproject: &[char] = &site.multiproject;
    let mut found: Vec<FoundVersion> = Vec::new();

    /* On bylist sites we expect that all projects has the same
     * format and that the regex to be applied is the one defined
     * at the site level. This is why we call the function
     * get_version_from_title_with_regex() with an empty program
     * level regex "" (same applies for filterout regex).
     */
    for entry in entries {
        let (title, published) = get_title_from_entry(entry);
        let mut title_split = Vec::new();
        let mut titles: Vec<&str> = Vec::new();

        if multiproject.is_empty() {
            titles.push(&title.content);
        } else {
            titles = title.content.split(multiproject).map(|t| t.trim()).collect();
        }

        for title in titles {
            debug!("{}: {} {}", site.name, title, &published);
            match get_version_from_title_with_regex(title, "", &site.regex, "", &site.filterout) {
                Ok((mname, mversion)) => {
                    if !mname.is_empty() {
                        title_split.push((mname, mversion));
                    }
                }
                Err(e) => {
                    eprintln!("Error: {e}");
                    eprintln!("\t{:#?} ({:#?})", title, &site.regex);
                }
            }
        }

        for (mname, mversion) in title_split {
            debug!("\tprogram: {} version: {} published: ({:#?})", mname, mversion, published);
            if !mname.is_empty() {
                for program in projects {
                    if program.name == mname {
                        match get_version_from_title_with_regex(&mversion, &program.regex, "", &program.filterout, "") {
                            Ok((_, rversion)) => {
                                if !rversion.is_empty() {
                                    found.push(get_foundversion_upon_entry(&mname, &rversion, &published, &program.entry, &site.entry));
                                }
                            }
                            Err(e) => {
                                eprintln!("Error: {e}");
                                eprintln!("\t{:#?} ({:#?})", title, &site.regex);
                            }
                        }
                    }
                }
            }
        }
    }

    found
}

fn get_versions_from_list_site(client: &Client, site: &Site) -> Result<Vec<FoundVersion>, Box<dyn Error>> {
    let url = &site.url;
    let projects = &site.projects;

    let entries: Vec<Entry> = get_entries_from_feed(client, url)?;

    debug!("{}: projects: {}, entries: {}", site.name, projects.len(), entries.len());

    let found = get_entries_and_projects_intersection(entries, projects, site);

    Ok(found)
}

pub fn read_site_config_file(filename: &str) -> Result<Sites, Box<dyn Error>> {
    debug!("File to load: {}", filename);

    let content = read_to_string(filename)?;
    let all_sites: Sites = serde_yml::from_str(&content)?;

    Ok(all_sites)
}

fn fetch_site_information(site: Site, cached: &mut CachedVersions, cache_path: &Path) {
    let trouves: &mut Vec<FoundVersion> = &mut Vec::new();
    let client = Client::new();

    match site.sitetype {
        SiteType::Project => {
            debug!("site (by project type): {} ({})", site.name, site.url);
            for program in &site.projects {
                match get_versions_from_byproject_site(&client, &site, program) {
                    Ok(found) => trouves.extend_from_slice(&found),
                    Err(e) => eprintln!("Error with '{}' site and program '{}': {}", site.url, program.name, e),
                }
            }
        }
        SiteType::List => {
            debug!("site (list type): {} ({})", site.name, site.url);
            match get_versions_from_list_site(&client, &site) {
                Ok(found) => *trouves = found,
                Err(e) => eprintln!("Error with '{}' site: {}", site.url, e),
            };
        }
    };

    /* Fills in the cache and prints to stdout if a newer version has been found */
    for trouve in trouves {
        debug!("{}: {} ({})", trouve.name, trouve.version, trouve.published);
        cached.update_cache(trouve);
    }

    /* Last checked date for all program in that cache is now() */
    cached.update_last_checked_date();

    /* write cache information to file.  */
    match write_cached_file(cache_path, cached) {
        Ok(_) => debug!("Successfully written cached file '{:?}'", cache_path),
        Err(error) => print_and_exit(&format!("Error writing cached file '{cache_path:?}': {error}"), 1),
    }
}

impl Sites {
    fn list_versions_in_cache(self, cache_path_passed: PathBuf, project: &str, date: bool) {
        let mut cached: CachedVersions = CachedVersions::new();
        let mut cache_path = cache_path_passed;

        for site in self.sites {
            /* Creating a name for the cache file */
            let filename = format!("{}.cache", site.name);
            cache_path.push(filename);

            /* reading the cache file if any */
            match read_cached_file(&cache_path) {
                Ok(c) => cached = c,
                Err(e) => print_and_exit(&format!("Error reading cached file for site '{}': {}", site.name, e), 1),
            };

            if project == "all" {
                println!("{cache_path:?}:");
            }
            print_cached_information(&cached, project, date);
            cache_path.pop();
        }
    }

    pub fn iterate_over_sites(self, list: Option<Option<String>>, date: bool, paths: ConfigPaths) {
        let mut handles = vec![];
        let project = match list {
            Some(p) => {
                if p.is_none() {
                    Some("all".to_string())
                } else {
                    p
                }
            }
            None => None,
        };
        if project.is_some() {
            if let Some(p) = project {
                self.list_versions_in_cache(paths.cache_path, &p, date);
            }
        } else {
            for site in self.sites {
                /* Creating a name for the cache file */
                let mut cache_path = paths.cache_path.clone();
                let filename = format!("{}.cache", site.name);
                cache_path.push(filename);

                /* Spawning one thread for each site to check assuming we
                 * have multiple checks to do at each site.
                 */
                let thread_join_handle = spawn(move || {
                    let mut cached: CachedVersions = CachedVersions::new();

                    /* reading the cache file if any */
                    match read_cached_file(&cache_path) {
                        Ok(c) => cached = c,
                        Err(e) => print_and_exit(&format!("Error reading cached file for site '{}': {}", site.name, e), 1),
                    };

                    /* Fetching each site upon their type */
                    fetch_site_information(site, &mut cached, &cache_path);
                });
                handles.push(thread_join_handle);
            }
            for handle in handles {
                let _res = handle.join();
            }
        }
    }
}
